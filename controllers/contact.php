<?php
require_once __DIR__ . '/../utils/utils.php';
require_once __DIR__ . '/../utils/File.php';
require_once __DIR__ . '/../entity/Contacto.php';
require_once __DIR__ . '/../database/Connection.php';
require_once __DIR__ . '/../repository/ContactRepository.php';
require_once __DIR__ . '/../exception/FileException.php';
require_once __DIR__ . '/../exception/QueryException.php';
require_once __DIR__ . '/../core/App.php';

$errores = [];
$mensaje = '';
$nombre = '';
$apellidos = '';
$email = '';
$asunto = '';

$config = require_once __DIR__ . '/../app/config.php';
App::bind('config', $config);
$contactRepository = new ContactRepository();


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nombre = trim(htmlspecialchars($_POST['nombre']));
    $apellidos = trim(htmlspecialchars($_POST['apellidos']));
    $email = trim(htmlspecialchars($_POST['email']));
    $asunto = trim(htmlspecialchars($_POST['asunto']));
    $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
    $foto = new File('imagen', $tiposAceptados);
    $foto->saveUploadFile(Contacto::RUTA_IMAGENES_GALLERY);
    if (empty($nombre))
        $errores[] = "El nombre no puede estar vacío";
    if (empty($email))
        $errores[] = "El email no puede estar vacío";
    else {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false)
            $errores[] = "El email no es válido";
    }
    if (empty($asunto))
        $errores[] = "El asunto no puede estar vacío";
    if (empty ($errores))
    $mensaje = "Los datos del formulario son correctos";

    $contacto = new Contacto($nombre, $apellidos, $email, $asunto,$foto->getFileName());
    $contactRepository->save($contacto);
    $mensaje = "Se han guardado los datos";
}
$usuarios = $contactRepository->findAll();

require __DIR__ . '/../views/contact.view.php';
