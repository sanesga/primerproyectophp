<?php
/**
 * Created by PhpStorm.
 * User: Sandra
 * Date: 17/12/2018
 * Time: 19:39
 */

require_once __DIR__ . '/../database/IEntity.php';

class Contacto implements IEntity
{
    const RUTA_IMAGENES_GALLERY= '../images/fotos/';
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $nombre;
    /**
     * @var string
     */
    private $apellidos;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $asunto;
    /**
     * @var string
     */
   private $foto;

    /**
     * Contacto constructor.
     * @param int $id
     * @param string $nombre
     * @param string $apellidos
     * @param string $email
     * @param string $asunto
     * @param string $foto
     */
    public function __construct(string $nombre='', string $apellidos='', string $email='', string $asunto='', string $foto='')
    {
        $this->id = null;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->email = $email;
        $this->asunto = $asunto;
        $this->foto = $foto;
    }

    public function toArray(): array{
        return[
            'nombre'=>$this->getnombre(),
            'apellidos'=>$this->getApellidos(),
            'email'=>$this->getEmail(),
            'asunto'=>$this->getAsunto(),
            'foto'=>$this->getFoto()

        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Contacto
     */
    public function setId(int $id): Contacto
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Contacto
     */
    public function setNombre(string $nombre): Contacto
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getApellidos(): string
    {
        return $this->apellidos;
    }

    /**
     * @param string $apellidos
     * @return Contacto
     */
    public function setApellidos(string $apellidos): Contacto
    {
        $this->apellidos = $apellidos;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Contacto
     */
    public function setEmail(string $email): Contacto
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getAsunto(): string
    {
        return $this->asunto;
    }

    /**
     * @param string $asunto
     * @return Contacto
     */
    public function setAsunto(string $asunto): Contacto
    {
        $this->asunto = $asunto;
        return $this;
    }

    /**
     * @return string
     */
    public function getFoto(): string
    {
        return $this->foto;
    }

    /**
     * @param string $foto
     * @return Contacto
     */
    public function setFoto(string $foto): Contacto
    {
        $this->foto = $foto;
        return $this;
    }


    /**
     * @return string
     */
    public function getUrlGallery(): string {
        return self::RUTA_IMAGENES_GALLERY.$this->getFoto();
    }



}