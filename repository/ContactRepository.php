<?php
/**
 * Created by PhpStorm.
 * User: Sandra
 * Date: 17/12/2018
 * Time: 19:37
 */

require_once __DIR__ . '/../database/QueryBuilder.php';
class contactRepository extends QueryBuilder
{
    public function __construct(string $table='contacto', $classEntity='Contacto')
    {
        parent::__construct($table, $classEntity);
    }
}